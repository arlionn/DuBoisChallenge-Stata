# Du Bois Challenge - The Stata Guide

This repository contains the Stata code for the Du Bois Challenge posted by @ajstarks here: https://github.com/ajstarks/dubois-data-portraits/tree/master/challenge. The details of the challenge are given here: https://scatter.wordpress.com/2021/02/22/bringing-communities-together-with-the-duboischallenge/. 

The aim of this challenge is to replicate the powerful visuals of Du Bois that he drew by hand during his seminal data work on the life of the African American community during the early 1900s in the USA. The challenge has 10 plates selected from the pool of visualizations. The complete selection of all of Du Bois' plates are given here: https://github.com/ajstarks/dubois-data-portraits. A Medium guide that explains the code is here: https://medium.com/the-stata-guide/graph-replication-the-du-bois-challenge-f0db93e719e6.

For the visualizations I am using the Rajdhani font available from Google fonts here: https://fonts.google.com/specimen/Rajdhani. If you find another font that is closer to the Du Bois typescript, then feel free to replace this one. Also please let me know about it as well!

The current visualization replicates some of the plates that provide a coding challenge. Others might be added here as well. 

I have marked out some of the code blocks from the dofiles. They have been left in there since I use them as intermediary steps to check the outputs. They also help with the learning process. Note that these files might be modified in the future for minor code improvements. Please check here for the latest file or clone the directory.


<img src="./challenge1/original-plate-07.jpg" height="400" title="Original">  <img src="./challenge1/dubois1_stata_plate7.png" height="400" title="Stata">

<img src="./challenge3/original-plate-27.jpg" height="400" title="Original">  <img src="./challenge3/dubois_stata_plate27.png" height="400" title="Stata">

<img src="./challenge4/original-plate-51.jpg" height="400" title="Original">  <img src="./challenge4/dubois4_stata_plate51.png" height="400" title="Stata">

<img src="./challenge6/original-plate-11.jpg" height="400" title="Original">  <img src="./challenge6/dubois6_stata_plate11.png" height="400" title="Stata">

<img src="./challenge7/original-plate-25.jpg" height="400" title="Original">  <img src="./challenge7/dubois7_stata_plate25.png" height="400" title="Stata">

<img src="./challenge9/original-plate-08.jpg" height="400" title="Original">  <img src="./challenge9/dubois9_stata_plate8.png" height="400" title="Stata">




